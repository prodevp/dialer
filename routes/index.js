var express = require('express');
var passport = require('passport');
var router = express.Router();
var multer  = require('multer');
var Contact  = require('../models/contacts');
var fs = require('fs');
var path = require('path');
// let upload = multer({ dest: '../public/images' });
/* Multer Config */
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve(__dirname+'/../public/images'))
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now())
  },
   onError : function(err, next) {
      cb(err);
    },
})
var upload = multer({ storage, storage })
// const upload = multer({ dest: '../public/images' })
router.get('/', function(req, res, next) {
    res.redirect('/login');
});

function isLoggedIn(req, res, next) {
    if (req.user) {
        next();
    } else {
        res.redirect('/login');
    }
}

// login get
router.get('/login', function(req, res, next) {
	if(req.user){
		res.redirect('/dashboard');
	}
    res.render('login', { message: '' });
});

// Login with configurations
router.post('/login', passport.authenticate('local-login', {
    successRedirect: '/dashboard', // redirect to the secure profile section
    failureRedirect: '/login', // redirect back to the signup page if there is an error
    failureFlash: true // allow flash messages
}));

// logout 
router.get('/logout', function(req, res){
	req.logout();
	res.redirect('/');
});


// dashboard page
router.get('/dashboard', isLoggedIn, async (req, res, next) => {
	let contacts = await Contact.find({});
	let contactData = contacts && contacts.length ? contacts : [];
	res.render('dashboard', { message: req.flash('loginMessage'), contacts});
});

router.get('/getcontacts', async (req, res, next) => {
	let contacts = await Contact.find({});
	res.json(contacts);
});

router.get('/add', function(req, res, next) {
    res.render('add', { message: '' });
});


// add contact
router.post('/add', upload.single('image'), async (req, res, next) => {
	const base64 = new Buffer(fs.readFileSync(req.file.path)).toString("base64");
	fs.unlink(req.file.path);
	let jsonContact = {
 		name: req.body.name,
	    url: req.body.url,
	    image: 'data:'+ req.file.mimetype + ';base64,' + base64,
	    phoneNumber: req.body.phoneNumber,
	};
 	 var newContact = new Contact(jsonContact);
	    newContact.save(function(err) {
        if (err)
            throw err;
         res.redirect('dashboard');
    });
});

// DELETE Contact
router.get('/delete/:id',isLoggedIn, function(req, res, next) {   
console.log(req.params) 
    var o_id = req.params.id.trim();
    Contact.deleteOne({_id: o_id}, (ddd,eerr) => {
    	res.redirect('/dashboard');
    });
})

// EDIT Contact
router.get('/edit/:id', isLoggedIn, async(req, res, next) => {
    var o_id = req.params.id.trim();
    let editData = await Contact.findOne({_id: o_id});
    res.render('edit', {message: '', editData: editData });
})

// EDIT Contact
router.post('/edit/:id',upload.single('image'), async(req, res, next) => {
    var o_id = req.params.id.trim();
    let base64;
    if(req.file && req.file.path){
    	base64 = 'data:'+ req.file.mimetype + ';base64,' + new Buffer(fs.readFileSync(req.file.path)).toString("base64");
    }else{
    	let contact = await Contact.findOne({_id: o_id});
    	base64 = contact.image;
    }
	if(base64){
		let jsonContact = {
	 		name: req.body.name,
		    url: req.body.url,
		    image: base64,
		    phoneNumber: req.body.phoneNumber,
		};
		Contact.findOneAndUpdate({_id: o_id}, { $set:
	            jsonContact },
	        (err, data) => {
	            if (err) {
	                console.log(err);
	            } else {
	               console.log('successfully updated!!!');
	            }
	            res.redirect('/dashboard');
	     });
	}else{
		console.log(`Can't converted to base64`);
	}
});




module.exports = router;