var mongoose = require('mongoose');

var contactSchema = mongoose.Schema({
    name: String,
    image: String,
    url: String,
    phone: Number,
    phoneNumber: Number
});

module.exports = mongoose.model('Contact', contactSchema);